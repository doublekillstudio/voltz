using UnityEngine;
using System.Collections;

public class UpdateScore : MonoBehaviour
{

	GameController gameManager;

	// Use this for initialization
	void Start ()
	{
		gameManager = GameController.instance;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GetComponent<TextMesh> ()) {
			GetComponent<TextMesh> ().text = gameManager.score.ToString ();
		}
		if (guiText) {
			guiText.text = gameManager.score.ToString ();
		}
	}
}
