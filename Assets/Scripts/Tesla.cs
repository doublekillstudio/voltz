﻿using UnityEngine;
using System.Collections;

public class Tesla : MonoBehaviour
{
	const float BOLT_ENDS_CORRECTION_FACTOR = 0.15f;

	// public vars
	public GameObject boltPrefab;
	public GameObject boltMissPrefab;
	public Transform shootOrigin;

	// configs
	float boltMaxDistance = 7;
	float shootDelay = 0.5f;

	// private
	bool isPowered = false;
	Tesla next = null;
	float timeSinceLastShoot;

	// index
	GameController controller;

	// Use this for initialization
	void Start ()
	{
		controller = GameController.instance;
		boltMaxDistance = controller.maxBoltDistance;
		shootDelay = controller.shootDelay;

		timeSinceLastShoot = Mathf.Infinity;
	}
	
	// Update is called once per frame
	void Update ()
	{
		timeSinceLastShoot += Time.deltaTime;
	}

	public void PowerOn ()
	{
		NewRotationIfHit ();
		if (!isPowered) {
			isPowered = true;
			GetComponent<Animator> ().SetTrigger ("on");
		}
	}

	void NewRotationIfHit () {
		RaycastHit2D hit = Physics2D.Raycast (shootOrigin.position, shootOrigin.up, boltMaxDistance);
		while (hit && hit.collider.GetComponent<Tesla> () != null) {
			Vector3 newRotation = new Vector3 (0, 0, Random.Range (controller.teslaMinRotation, controller.teslaMaxRotation));
			transform.rotation = Quaternion.Euler (newRotation);
			hit = Physics2D.Raycast (shootOrigin.position, shootOrigin.up, boltMaxDistance);
		}
	}
	
	public bool powered {
		get	{ return isPowered;}
	}

	public bool connected {
		get	{ return next != null;}
	}

	public void Shoot ()
	{
		if (timeSinceLastShoot < shootDelay) {
			return;
		} else {
			timeSinceLastShoot = 0f;
		}

		if (collider2D) {
			collider2D.enabled = false;
		}

		RaycastHit2D hit = Physics2D.Raycast (shootOrigin.position, shootOrigin.up, boltMaxDistance);

		Tesla other = null;
		if (hit) {
			other = hit.collider.GetComponent<Tesla> ();
			if (other) {
				next = other;
				other.PowerOn ();
				GetComponent<Animator> ().SetTrigger ("hit");
				AudioController.Play ("hit");
			}
		}

		Vector3 position = Vector3.MoveTowards (shootOrigin.position, transform.position, BOLT_ENDS_CORRECTION_FACTOR);
		position.z = -1;
		Quaternion rotation = shootOrigin.rotation;
		if (hit) {
			// create the bolt
			GameObject obj = (GameObject)GameObject.Instantiate (boltPrefab.gameObject, position, rotation);
			// adjust the effect to link this tesla with target tesla
			LightningEffect effect = obj.GetComponent<LightningEffect> ();
			if (effect) {
				Vector3 targetPos = Vector3.MoveTowards (hit.point, other.transform.position, BOLT_ENDS_CORRECTION_FACTOR);
				targetPos.z = -1;
				effect.target.position = targetPos;
			}
		} else {
			// create the missed bolt
			GameObject obj = (GameObject)GameObject.Instantiate (boltMissPrefab.gameObject, position, rotation);
			// adjust the effect to reach max distance and vanish
			LightningEffect effect = obj.GetComponent<LightningEffect> ();
			if (effect) {
				// bolt goes max distance in the direction we are facing
				Vector3 targetPos = position + rotation * Vector3.up * (boltMaxDistance + BOLT_ENDS_CORRECTION_FACTOR);
				targetPos.z = -1;
				effect.target.position = targetPos;

				// discover if we missed by a little bit, which can cause the effect
				// to overlap the target, if not constrained
				float radius = effect.maxDisplacement;
				RaycastHit2D almostHit = Physics2D.CircleCast (shootOrigin.position, radius, shootOrigin.up, boltMaxDistance - 2 * effect.maxDisplacement);
				if (almostHit) {
					// if we almost hit, constrain the effect accordingly
					if (transform.InverseTransformDirection(almostHit.normal).x > 0) {
						// missed in left side
						effect.constraint = LightningEffect.Constraint.RightOnly;
					} else {
						// missed in left side
						effect.constraint = LightningEffect.Constraint.LeftOnly;
					}
				} else {
					effect.constraint = LightningEffect.Constraint.None;
				}
			}
			// destroys the bolt just before we can shoot again
			GameObject.Destroy (obj, shootDelay);
		}
		AudioController.Play ("spark");
		 
		// debug
		//Debug.DrawLine (shootOrigin.position, shootOrigin.position + shootOrigin.up * boltMaxDistance, Color.red, 1);
		//if (hit) {
		//	Debug.DrawRay (hit.point, hit.normal, Color.green, 2);
		//}
	}

}
