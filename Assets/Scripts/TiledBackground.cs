﻿using UnityEngine;
using System.Collections;

public class TiledBackground : MonoBehaviour
{



	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void LateUpdate ()
	{

		float y = Camera.main.transform.position.y;
		float x = transform.position.x;
		float z = transform.position.z;
		
		transform.position = new Vector3 (x, y, z);

		float offSetY = y / transform.lossyScale.y;
		float offSetX = renderer.material.mainTextureOffset.x;
		renderer.material.mainTextureOffset = new Vector2 (offSetX, offSetY);
	}
}
