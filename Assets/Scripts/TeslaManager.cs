using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class TeslaManager : MonoBehaviour
{
	public Tesla template;
	public Vector2 startPosition;

	float distance;
	float teslaXMin;
	float teslaXMax;
	float minRotation;
	float maxRotation;

	bool started = false;

	// indexes
	GameController controller;

	// All existing shooters
	List<Tesla> _teslas;

	private Tesla[] teslas {
		get { return _teslas.ToArray (); }
	}

	public Tesla GetLastOfChain ()
	{
		var poweredTeslas = 
			from t in teslas
				where t.powered
				select t;
		Tesla lastPoweredTesla = poweredTeslas.LastOrDefault ();
		return lastPoweredTesla;
	}

	public void StartTeslaManager () {
		controller = GameController.instance;
		
		minRotation = controller.teslaMinRotation;
		maxRotation = controller.teslaMaxRotation;
		distance = controller.distanceBetweenTeslas;
		teslaXMin = controller.teslaXMin;
		teslaXMax = controller.teslaXMax;

		// create the first shooter at 0,0
		// and starts it
		Tesla first = CreateTeslaAtXY (startPosition.x, startPosition.y);
		first.PowerOn ();
		_teslas = new List<Tesla> ();
		_teslas.Add (first);
		started = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!started) {
			return;
		}

		// from the last shooter position, create a shooter in
		// each visible camera position, plus one not yet visible
		Camera mainCamera = Camera.main;
		Tesla lastShooter = _teslas.Last ();
		float maxVisibleY = mainCamera.transform.position.y + mainCamera.orthographicSize;
		float yPosition = lastShooter.transform.position.y;
		while (yPosition < maxVisibleY) {
			// this increment after testing if position is in range
			// causes the "plus one not yet visible"
			yPosition += distance;
			Tesla s = CreateTeslaAtYWithRandomX (yPosition);
			_teslas.Add (s);
		}

		// from the first shooter in the list, removes all shooters 
		// not visible by camera (plus a threshold)
		float threshold = 1.0f;
		float minVisibleY = mainCamera.transform.position.y - mainCamera.orthographicSize - threshold;
		var fistShooter = _teslas [0];
		while (fistShooter.transform.position.y < minVisibleY) {
			GameObject.Destroy (fistShooter.gameObject);
			_teslas.RemoveAt (0);
			fistShooter = _teslas [0];
		}

	}

	Tesla CreateTeslaAtYWithRandomX (float yPosition)
	{
		float xPosition = Random.Range (teslaXMin, teslaXMax);
		return CreateTeslaAtXY (xPosition, yPosition);
	}

	Tesla CreateTeslaAtXY (float xPosition, float yPosition)
	{
		Vector3 position = new Vector3 (xPosition, yPosition);
		Vector3 rotation = new Vector3 (0, 0, Random.Range (minRotation, maxRotation));
		//
		GameObject obj = (GameObject)GameObject.Instantiate (template.gameObject, position, Quaternion.Euler (rotation));
		//
		Tesla shooter = obj.GetComponent<Tesla> ();
		return shooter;
	}

	public static TeslaManager Instance () {
		return FindObjectOfType<TeslaManager> ();
	}

	public void Initiate () {
		CleanTeslas ();
		StartTeslaManager ();
	}

	public void Stop () {
		CleanTeslas ();
		started = false;
	}

	void CleanTeslas () {
		Tesla[] teslas = FindObjectsOfType<Tesla> ();
		foreach (var tesla in teslas) {
			Destroy (tesla.gameObject);
		}
		if (_teslas != null) {
			_teslas.Clear ();
			_teslas = null;
		}
	}
}
