﻿using UnityEngine;
using System.Collections;

public class TeslaDebugShoot : MonoBehaviour {

	Tesla tesla;

	// Use this for initialization
	void Start () {
		tesla = GetComponent<Tesla>();
		tesla.PowerOn();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			Debug.Log("Debug Shoot!!");
			tesla.Shoot ();
		}
	}
}
