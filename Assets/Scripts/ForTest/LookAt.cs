﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour
{
	public Transform target;

	// Update is called once per frame
	void Update ()
	{
		if (target) {
			Vector3 mappedXZ = target.position - transform.position;
			mappedXZ.z = mappedXZ.y;
			mappedXZ.y = 0;

			var newRotation = Quaternion.LookRotation(mappedXZ).eulerAngles;
			newRotation.z = -newRotation.y;
			newRotation.y = 0;

			transform.eulerAngles = newRotation;
		}
	}
}
