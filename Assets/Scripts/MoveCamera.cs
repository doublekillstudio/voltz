﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour
{
	// config
	float speed = 2.0f;

	// index
	GameController controller;
	Camera mainCamera;

	// Use this for initialization
	void Start ()
	{
		controller = GameController.instance;
		mainCamera = Camera.main;
	}
	
	// Update is called once per frame
	void LateUpdate ()
	{
		if (controller.state == GameController.State.Running) {
			float step = Time.deltaTime * speed * controller.speedMultiplier;

			//float targetX = mainCamera.transform.position.x;
			//float targetZ = mainCamera.transform.position.z;
			//float targetY = mainCamera.transform.position.y + step;
			
			//mainCamera.transform.position = new Vector3 (targetX, targetY, targetZ);
			mainCamera.transform.Translate(0, step, 0);
		}
	}
}
