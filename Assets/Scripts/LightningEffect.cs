﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LightningEffect : MonoBehaviour
{
	public enum Constraint
	{
		None,
		LeftOnly,
		RightOnly
	}

	public Transform target;
	public int zigs = 25;
	public float speed = 1f;
	public float maxDisplacement = 1f;
	public float varChance = 0.1f;
	public Constraint constraint = Constraint.None;

	// private
	private ParticleSystem.Particle[] particles;
	float[] displacements;

	delegate void UpdateFunctionDelegate ();

	void Start ()
	{
		particleSystem.enableEmission = false;
		particleSystem.Emit (zigs);
		particles = new ParticleSystem.Particle [zigs];
		particleSystem.GetParticles (particles);

		displacements = new float[zigs];
		MipointDisplacementFloat (displacements, maxDisplacement, 1.0f);
	}

	void Update ()
	{
		if (!target) 
			return;
#if UNITY_EDITOR
		if (particles == null) {
			particleSystem.Emit (zigs);
			particles = new ParticleSystem.Particle [zigs];
			particleSystem.GetParticles (particles);
		}
#endif
		Vector3 targetPosition = transform.InverseTransformPoint (target.position);
		Vector3 perpendicular = Vector3.Cross (targetPosition, Vector3.forward).normalized;
		// > cross product left hand rule
		// cross(a, b) = result
		//     +z
		//     | b
		//     |__ result
		//  a /    +x
		//  +y

		MipointDisplacementFloat (displacements, maxDisplacement, varChance);
		if (constraint == Constraint.RightOnly) {
			for (int i = 0; i < displacements.Length; i++) {
				displacements [i] = Mathf.Abs (displacements [i]);
			}
		} else if (constraint == Constraint.LeftOnly) {
			for (int i = 0; i < displacements.Length; i++) {
				displacements [i] = -Mathf.Abs (displacements [i]);
			}
		}

		float oneOverZigs = 1f / (float)zigs;
		for (int i=0; i < particles.Length; i++) {
			Vector3 position = Vector3.Lerp (Vector3.zero, targetPosition, oneOverZigs * (float)i);
			position += perpendicular * displacements [i];
			particles [i].position = position;
		}

		particleSystem.SetParticles (particles, zigs);
	}

	void MipointDisplacementFloat (float[] points, float displacement, float varChance)
	{
		MidpointDisplacementFloatInner (points, 0, points.Length - 1, displacement, varChance);
	}

	void MidpointDisplacementFloatInner (float[] points, int start, int end, float displacement, float varChance)
	{
		// stop when the difference between indexes is
		// one or less (i.e. there are no point between them)
		int diff = end - start;
		if (diff <= 1) {
			return;
		}
		
		// gets the midpoint ofset index, relative to start,
		// rounded to int
		int midPointOffset = Mathf.RoundToInt (diff / 2.0f);
		// gets the fractional normalized offset between [0..1]
		float normalizedOffset = midPointOffset / (float)diff;
		
		// gets the midpoint index
		int mid = start + midPointOffset;
		
		if (varChance >= 1.0f || Random.value <= varChance) {
			varChance = 1.0f;
			
			// gets the midpoint value
			//Vector3 midPointValue = Vector3.Lerp (points [start], points [end], normalizedOffset);
			float midPointValue = Mathf.Lerp (points [start], points [end], normalizedOffset);

			// displaces the midpoint value using random
			float random = Random.Range (-1f, 1f);
			float ratio = random;
			
			midPointValue += ratio * displacement;
			
			// set the value in the array
			points [mid] = midPointValue;
		}
		
		// recursivelly call to each half
		MidpointDisplacementFloatInner (points, start, mid, normalizedOffset * displacement, varChance);
		MidpointDisplacementFloatInner (points, mid, end, (1f - normalizedOffset) * displacement, varChance);
	}
	
	/*
	void MipointDisplacement (Vector3[] points, float displacement, float varChance)
	{
		MidpointDisplacementInner (points, 0, points.Length - 1, displacement, varChance);
	}

	void MidpointDisplacementInner (Vector3[] points, int start, int end, float displacement, float varChance)
	{
		// stop when the difference between indexes is
		// one or less (i.e. there are no point between them)
		int diff = end - start;
		if (diff <= 1) {
			return;
		}

		// gets the midpoint ofset index, relative to start,
		// rounded to int
		int midPointOffset = Mathf.RoundToInt (diff / 2.0f);
		// gets the fractional normalized offset between [0..1]
		float normalizedOffset = midPointOffset / (float)diff;
		
		// gets the midpoint index
		int mid = start + midPointOffset;

		if (varChance >= 1.0f || Random.value <= varChance) {
			varChance = 1.0f;

			// gets the midpoint value
			Vector3 midPointValue = Vector3.Lerp (points [start], points [end], normalizedOffset);
			
			// displaces the midpoint value using random
			float random = Random.Range (-1f, 1f);
			float ratio = random;
			
			midPointValue.x += ratio * displacement;
			
			// set the value in the array
			points [mid] = midPointValue;
		}

		// recursivelly call to each half
		MidpointDisplacementInner (points, start, mid, normalizedOffset * displacement, varChance);
		MidpointDisplacementInner (points, mid, end, (1f - normalizedOffset) * displacement, varChance);
	}

	/*
	static float NORMSDIST (float z_score)
	{
		float z_ = - ((z_score * z_score) / 2);
		float normDist = (1 / ((Mathf.Sqrt (2 * Mathf.PI)))) * (Mathf.Exp (z_));
		return normDist;
	}

	/*
	void UpdatePerlinNoise ()
	{
		if (perlin == null) {
			perlin = new Perlin ();
		}
		
		particleSystem.GetParticles (particles);
		
		float time = Time.time * speed;
		
		for (int i=0; i < particles.Length; i++) {
			Vector3 position = Vector3.Lerp (Vector3.zero, target.localPosition, oneOverZigs * (float)i);
			
			// calculate displacement for this particle,
			// make displacement follow normal distribution
			float zScore = i * oneOverZigs * 4 - 2;
			float displacement = NORMSDIST (zScore) / normHeight;
			
			// diplacement ratio based in perlin noise
			float noisex01 = Mathf.PerlinNoise (position.y * 2, time);
			float ratiox = Mathf.Lerp (-1f, 1f, Mathf.InverseLerp (0, 1, noisex01));
			
			float noisey01 = Mathf.PerlinNoise (17f + position.y * 4, time);
			float ratioy = Mathf.Lerp (-1f, 1f, Mathf.InverseLerp (0, 1, noisey01));
			
			float noises01 = Mathf.PerlinNoise (37f + position.y, time);
			float ratios = noises01;
			
			// add displacement to x
			position.x += ratiox * displacement * scale;
			position.y += ratioy * displacement * scale / 2f;
			float startSize = particleSystem.startSize;
			float size = startSize * (1 + ratios);
			
			// set particle
			particles [i].position = position;
			particles [i].size = size;
			particles [i].lifetime = 1.0f;
		}
		
		particleSystem.SetParticles (particles, zigs);
	}

	void Update02 ()
	{
		if (perlin == null) {
			perlin = new Perlin ();
		}
		particleSystem.GetParticles (particles);
		
		float time = Time.time * speed;
		
		for (int i=0; i < particles.Length; i++) {
			Vector3 position = Vector3.Lerp (Vector3.zero, target.localPosition, oneOverZigs * (float)i);
			
			//float offset = noise.Noise (Time.time,  position.x, position.y);
			float offsetx = perlin.Noise (time, position.y);
			float offsety = perlin.Noise (7 + time * 6 / 7, position.y);
			float offsetsize = perlin.Noise (13 + time * 12 / 13, position.y);
			
			// make displacement bigger in center
			// [    0 .. 1   ]
			float ratio = 0.2f + NORMSDIST ((i * oneOverZigs) * 4 - 2) / NORMSDIST (0);
			// [ -0.5 .. 0.5 ]
			//ratio -= 0.5f;
			// [  0.5 .. 0 .. 0.5 ]
			//ratio = Mathf.Abs (ratio);
			// [    0 .. 1 .. 0   ]
			//ratio = (0.5f - ratio) * 2;
			
			position.x += offsetx * ratio * scale;
			position.y += offsety * ratio * scale;
			
			particles [i].size = particleSystem.startSize * (1f + offsetsize * 1f);
			particles [i].position = position;
			particles [i].velocity = Vector3.zero;
			particles [i].lifetime = 1.0f;
		}
		
		particleSystem.SetParticles (particles, zigs);
	}

	static float Phi (float x)
	{
		// constants
		const float a1 = 0.254829592f;
		const float a2 = -0.284496736f;
		const float a3 = 1.421413741f;
		const float a4 = -1.453152027f;
		const float a5 = 1.061405429f;
		const float p = 0.3275911f;
		const float sqrt2 = 1.414213562f;
		
		float x_ = Mathf.Abs (x) / sqrt2;
		
		// A&S formula 7.1.26
		float t = 1.0f / (1.0f + p * x_);
		float y = 1.0f - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Mathf.Exp (-x_ * x_);
		
		return 0.5f * (1.0f + Mathf.Sign (x) * y);
	}

	void Update01 ()
	{
		if (perlin == null) {
			perlin = new Perlin ();
		}
		int count = particleSystem.GetParticles (particles);
		
		float timex = Time.time * speed * 0.1365143f;
		float timey = Time.time * speed * 1.21688f;
		float timez = Time.time * speed * 2.5564f;
		
		for (int i=0; i < particles.Length; i++) {
			Vector3 position = Vector3.Lerp (Vector3.zero, target.localPosition, oneOverZigs * (float)i);
			Vector3 offset = new Vector3 (perlin.Noise (timex + position.x, timex + position.y, timex + position.z),
			                          perlin.Noise (timey + position.x, timey + position.y, timey + position.z),
			                          perlin.Noise (timez + position.x, timez + position.y, timez + position.z));
			position += (offset * scale * ((float)i * oneOverZigs));
			
			particles [i].position = position;
			//particles [i].color = Color.white;
			particles [i].rotation = 0f;
			particles [i].lifetime = 1.0f;
			//Debug.Log (particles [i].lifetime);
			//particles[i].energy = 1f;
		}

		particleSystem.SetParticles (particles, zigs);

	}
	*/

	void OnDrawGizmos ()
	{
		//Color whiteA50 = new Color (1f, 1f, 1f, 0.25f);
		//Gizmos.color = whiteA50;

		//Gizmos.DrawSphere (transform.position, 0.5f);
		//Gizmos.DrawSphere (target.position, 0.5f);

		if (target) {
			Gizmos.color = Color.green;
			Gizmos.DrawWireCube (transform.position, Vector3.one * 0.1f);
			Gizmos.DrawLine (transform.position, target.position);
			Gizmos.color = Color.blue;
			Gizmos.DrawWireCube (target.position, Vector3.one * 0.1f);
		} else {
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube (transform.position, Vector3.one * 0.1f);
		}
	}
}
