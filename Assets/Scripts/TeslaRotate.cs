﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Tesla))]
public class TeslaRotate : MonoBehaviour
{
	// configs
	float angularSpeed;
	float maxRotation;
	float minRotation;

	// state
	float rotation = 0f;

	// indexes
	Tesla tesla;
	GameController controller;

	// Use this for initialization
	void Start ()
	{
		controller = GameController.instance;
		angularSpeed = controller.teslaAngularSpeed;
		minRotation = controller.teslaMinRotation;
		maxRotation = controller.teslaMaxRotation;

		rotation = transform.rotation.eulerAngles.z;
		// gets rotation between -180 && 180
		rotation = ((rotation + 180f) % 360f) - 180f;
		// adjust the direction of rotation to point
		// toward the nearest edge
		angularSpeed *= Mathf.Sign (rotation);
		tesla = GetComponent<Tesla> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (tesla.powered && !tesla.connected) {
			
			float slice = angularSpeed * Time.deltaTime * controller.speedMultiplier;
			rotation += slice;
			float rotation_l = Mathf.Clamp (rotation, minRotation, maxRotation);
			if (rotation_l != rotation) {
				angularSpeed = -angularSpeed;
				rotation = rotation_l;
			}

			transform.rotation = Quaternion.Euler (Vector3.forward * rotation);
		}
	}
}
