﻿using UnityEngine;
using System.Collections;

public class AdsManager : MonoBehaviour
{
#if UNITY_IPHONE

	private ADBannerView banner = null;
	
	void Start()
	{
		banner = new ADBannerView(ADBannerView.Type.Banner, ADBannerView.Layout.Bottom);
		ADBannerView.onBannerWasLoaded  += OnBannerLoaded;
	}
	
	void OnBannerLoaded()
	{
		banner.visible = true;
	}

#endif
}
