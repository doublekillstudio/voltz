﻿using UnityEngine;
using System.Collections;

public class ScoreText : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		
		int highScore = PlayerPrefs.GetInt ("Score", 0);
		
		if (GetComponent<TextMesh> ()) {
			GetComponent<TextMesh> ().text = highScore.ToString ();
		}
		if (guiText) {
			guiText.text = highScore.ToString ();
		}
	}

}
