using UnityEngine;
using System.Linq;

public class GameController : MonoBehaviour
{
	public enum State
	{
		Null,
		Running,
		GameOver
	}

	public int score = 0;
	public float speedMultiplier = 1f;
	public float speedStep = 0.01f;
	public State state = State.Null;

	// configs
	public float cameraSpeed = 2.0f;
	public float teslaAngularSpeed = 90f;
	public float teslaMinRotation = -70f;
	public float teslaMaxRotation = +70f;
	public float distanceBetweenTeslas = 3.0f;
	public float teslaXMin = -4.0f;
	public float teslaXMax = +4.0f;
	public float maxBoltDistance = 4.0f;
	public float shootDelay = 0.25f;

	// private
	Vector3 cameraOriginalPosition;
	public int _score;
	public float _speedMultiplier;
	public float _speedStep;

	// find the first GameController object with tag GameController in scene
	public static GameController instance {
		get {
			return GameObject.FindWithTag ("GameController").GetComponent<GameController> ();
		}
	}

	// private
	TeslaManager teslaManager;

	// Use this for initialization
	void Start ()
	{
		teslaManager = GetComponent<TeslaManager> ();
		if (!teslaManager) {
			Debug.LogError ("Component TeslaManager not found");
		}

		// save camera starting position
		cameraOriginalPosition = Camera.main.transform.position;

		// save initial values
		_score = score;
		_speedMultiplier = speedMultiplier;
		_speedStep = speedStep;
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch (state) {
			case State.Running: UpdateRunning (); break;
		}
	}

	void UpdateRunning ()
	{
		Tesla last = teslaManager.GetLastOfChain ();
		
		if (last) {
			if (Input.GetButtonDown ("Fire1")) {
				last.Shoot ();
				Tesla next = teslaManager.GetLastOfChain ();
				if (last != next) {
					IncrementScore ();
				}
			}
		} else {
			TransitionGameOver ();
		}
	}

	void IncrementScore ()
	{
		score++;
		speedMultiplier += speedStep;
	}

	void TransitionGameOver () {
		SetScore ();
		state = State.GameOver;
		TeslaManager.Instance ().Stop ();
		UIController.instance.Enable (true);
		Camera.main.transform.position = cameraOriginalPosition;
	}

	public void TransitionInitiate () {
		// reload initial values
		score = _score;
		speedMultiplier = _speedMultiplier;
		speedStep = _speedStep;

		// restart state
		UIController.instance.Disable ();
		state = State.Running;
		TeslaManager.Instance ().Initiate ();
	}

	void SetScore () {
		PlayerPrefs.SetInt ("Score", score);
		if (Leaderboards.isUserAuthenticated) {
			Leaderboards.ReportScore (score);
		}
		int highScore = PlayerPrefs.GetInt ("HighScore", 0);
		if (score > highScore) {
			PlayerPrefs.SetInt ("HighScore", score);
			PlayerPrefs.SetInt ("NewHighScore", 1);
		} else {
			PlayerPrefs.SetInt ("NewHighScore", 0);
		}
	}
}
