﻿using UnityEngine;
using System.Collections;

public class Leaderboards : MonoBehaviour
{
	const string leaderboardID = "voltz.HighScores";

	public static void Authenticate ()
	{
		if (!Social.localUser.authenticated) {
			Social.localUser.Authenticate (success => {
				if (success) {
					Debug.Log ("Authentication successful");
					
				} else {
					Debug.Log ("Authentication failed");
				}	
			});
		}
	}

	public static bool isUserAuthenticated {
		get {
			return Social.localUser.authenticated;
		}
	}

	public static void ReportScore (long score)
	{
		if (Social.localUser.authenticated) {
			Debug.Log ("Reporting score " + score + " on leaderboard " + leaderboardID);
			Social.ReportScore (score, leaderboardID, success => {
				Debug.Log (success ? "Reported score successfully" : "Failed to report score");
			});
		}
	}

	public static void ShowLeaderboard ()
	{
		if (Social.localUser.authenticated) {
			Debug.Log ("Showing leaderboard UI");
			Social.ShowLeaderboardUI ();
		}
	}
}
