﻿using UnityEngine;
using System.Collections;

public class DemoScore : MonoBehaviour
{
    public int min = 5;
    public int max = 50;
    public float interval = 0.25f;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine ("UpdateDemoScore");
    }

    IEnumerator UpdateDemoScore ()
    {
        GameController controller = GameController.instance;
        while (true) {
            Debug.Log ("UpdateDemoScore ()");
            if (controller.state == GameController.State.Running) {
                int value = Random.Range (min, max);
                controller.score = value;
            }
            yield return new WaitForSeconds (interval);
        }
    }
}
