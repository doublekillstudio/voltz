﻿using UnityEngine;
using System.Collections;

public class DestroyBoltOutOfView : MonoBehaviour
{
	LightningEffect bolt;

	void Start ()
	{
		bolt = GetComponent<LightningEffect> ();
		Destroy (gameObject, 10.0f);
	}

	// Update is called once per frame
	void Update ()
	{
		Camera mainCamera = Camera.main;

		float threshold = 0.1f;
		float minVisibleY = mainCamera.transform.position.y - mainCamera.orthographicSize - threshold;
		if (transform.position.y < minVisibleY && bolt.target.position.y < minVisibleY) {
			GameObject.Destroy (gameObject);
		}

	}
}
