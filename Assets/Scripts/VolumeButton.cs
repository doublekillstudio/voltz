﻿using UnityEngine;
using System.Collections;

public class VolumeButton : MonoBehaviour
{
	public SpriteRenderer muteCommandOverlay;

	// Use this for initialization
	void Start ()
	{
		AdjustSprite ();
	}

	void OnMouseDown ()
	{
		AudioController.mute = !AudioController.mute;
		AdjustSprite ();
	}

	void AdjustSprite ()
	{
		if (muteCommandOverlay) {
			muteCommandOverlay.enabled = AudioController.mute;
		}
	}
}
