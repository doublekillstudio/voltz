﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class AudioController : MonoBehaviour
{
	public AudioSource hit;
	public AudioSource spark;

	// private
	bool _mute = false;
	static AudioController controller;

	static AudioController GetInstance ()
	{
		if (!controller) {
			controller = FindObjectOfType<AudioController> ();
		}
		return controller;
	}

	public static void Play (string sound)
	{
		GetInstance ().PlayAudio (sound);
	}

	void Start ()
	{	
		int prefMute = PlayerPrefs.GetInt ("AudioMute", 0);
		_mute = (prefMute != 0);
	}

	public static bool mute {
		get {
			return GetInstance ()._mute;
		}
		set {
			PlayerPrefs.SetInt ("AudioMute", value ? 1 : 0);
			GetInstance ()._mute = value;
		}
	}

	void PlayAudio (string sound)
	{
		if (mute) {
			return;
		}
		switch (sound) {
		case "hit":
			GetInstance ().hit.Play ();
			break;
		case "spark":
			GetInstance ().spark.Play ();
			break;
		default:
			break;
		}
	}
}
