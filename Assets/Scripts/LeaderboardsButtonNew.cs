﻿using UnityEngine;
using System.Collections;

public class LeaderboardsButtonNew : MonoBehaviour
{
    public SpriteRenderer[] buttonViews = new SpriteRenderer[]{};

    void Start ()
    {
#if UNITY_IPHONE
        Leaderboards.Authenticate ();
#elif UNITY_WEBPLAYER
        if (buttonViews != null) {
            foreach (var renderer in buttonViews) {
                renderer.enabled = false;
            }
        }
#endif
    }

#if UNITY_IPHONE
    void Update ()
    {

        if (buttonViews != null) {
            var isUserAuthenticated = Leaderboards.isUserAuthenticated;
            foreach (var renderer in buttonViews) {
                renderer.enabled = isUserAuthenticated;
            }
        }
    }

    void OnMouseDown ()
    {
        if (Leaderboards.isUserAuthenticated) {
            Leaderboards.ShowLeaderboard ();    
        }
    }
#endif
}
