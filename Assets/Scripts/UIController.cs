﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour
{
		public static UIController instance;
		GameObject gameScore;
		GameObject startBolt;
		TextMesh score;
		TextMesh best;
		TextMesh bestBig;
		TextMesh label;

		void Awake ()
		{
				instance = this;
				gameScore = GameObject.Find ("Game Score");
				startBolt = GameObject.Find ("Start Bolt");
				score = GameObject.Find ("CurrentScore").GetComponent<TextMesh> ();
				best = GameObject.Find ("BestScore").GetComponent<TextMesh> ();
				bestBig = GameObject.Find ("BestScoreBig").GetComponent<TextMesh> ();
				label = GameObject.Find ("Label").GetComponent<TextMesh> ();
		}

		void Start ()
		{
				Enable ();
		}

		public void Enable (bool gameOverMode = false)
		{
				if (gameOverMode) {
						label.text = "Game Over!";
						score.text = "Score: " + GameController.instance.score;
						best.text = "Best: " + PlayerPrefs.GetInt ("HighScore", 0);
						bestBig.text = "";
				} else {
						label.text = "Best";
						bestBig.text = "" + PlayerPrefs.GetInt ("HighScore", 0);
						score.text = "";
						best.text = "";
				}
				gameScore.SetActive (false);
				startBolt.SetActive (false);
				gameObject.SetActive (true);
		}

		public void Disable ()
		{
				gameObject.SetActive (false);
				startBolt.SetActive (true);
				gameScore.SetActive (true);
				AudioController.Play ("spark");
				AudioController.Play ("hit");
		}
}
